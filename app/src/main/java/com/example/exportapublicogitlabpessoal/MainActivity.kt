package com.example.exportapublicogitlabpessoal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.modulo.ClasseModulo

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val tv : TextView = findViewById(R.id.text)
        tv.text = ClasseModulo().string
    }
}